SELECT [DateTime], [Value], [TagName]
FROM [Runtime].[dbo].[History] 
Where (Tagname='Power_P_1_7_0_W6.CV')
    and ([DateTime] >= '2024-02-01 00:00:00' and [DateTime] < '2024-03-31 00:00:00')
    and ([Value] IS NOT NULL)
