SELECT [DateTime], [Value], [TagName]
FROM [Runtime].[dbo].[History]
Where (TagName = 'Energy_Ameno_2_8_0_BBB6004.Wh')
    and (TagName = 'Energy_Apiu_1_8_0_BBB6004.Wh')
    and (TagName = 'Energy_Rmeno_4_8_0_BBB6004.VArh')
    and (TagName = 'Energy_Rpiu_3_8_0_BBB6004.VArh')
    and (TagName = 'Energy_Ameno_2_8_0_BBB6017.Wh')
    and (TagName = 'Energy_Apiu_1_8_0_BBB6017.Wh')
    and (TagName = 'Energy_Rmeno_4_8_0_BBB6017.VArh')
    and (TagName = 'Energy_Rpiu_3_8_0_BBB6017.VArh')
    and ([DateTime] >= '2023-12-01 00:00:00' and [DateTime] < '2024-01-01 00:00:00')
    and ([Value] IS NOT NULL)
