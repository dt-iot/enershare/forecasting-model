SELECT [DateTime], [Value], [TagName]
FROM [Runtime].[dbo].[History] 
Where (Tagname='Humidity_NCD1.Cv') 
    and (Tagname='Humidity_NCD10.Cv')
    and (Tagname='Humidity_NCD2.Cv')
    and (Tagname='Humidity_NCD3.Cv')
    and (Tagname='Humidity_NCD4.Cv')
    and (Tagname='Humidity_NCD5.Cv')
    and (Tagname='Humidity_NCD6.Cv')
    and (Tagname='Humidity_NCD7.Cv')
    and (Tagname='Humidity_NCD8.Cv')
    and (Tagname='Humidity_NCD9.Cv')
    and ([DateTime] >= '2023-12-01 00:00:00' and [DateTime] < '2024-01-01 00:00:00')
    and ([Value] IS NOT NULL)