SELECT [DateTime], [Value], [TagName]
FROM [Runtime].[dbo].[History] 
Where (Tagname='Energy_Ameno_2_8_0_BBB6164.Wh')
    and (Tagname='Energy_Apiu_1_8_0_BBB6164.Wh')
    and (Tagname='Energy_Rmeno_4_8_0_BBB6164.VArh')
    and (Tagname='Energy_Rpiu_3_8_0_BBB6164.VArh')
    and ([DateTime] >= '2023-12-01 00:00:00' and [DateTime] < '2024-01-01 00:00:00')
    and ([Value] IS NOT NULL)