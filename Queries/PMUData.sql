SELECT [DateTime], [Value], [TagName]
FROM [Runtime].[dbo].[History]
Where (TagName = 'MBT1.L1_V_Potenza_Attiva_A_Fase_R​')
    and (TagName = 'MBT1.L1_V_Potenza_Attiva_A_Fase_S')
    and (TagName = 'MBT1.L1_V_Potenza_Attiva_A_Fase_T')
    and (TagName = 'MBT1.L1_V_Potenza_Attiva_E_Fase_R')
    and (TagName = 'MBT1.L1_V_Potenza_Attiva_E_Fase_S')
    and (TagName = 'MBT1.L1_V_Potenza_Attiva_E_Fase_/')
    and ([DateTime] >= '2023-12-01 00:00:00' and [DateTime] < '2024-01-01 00:00:00')
    and ([Value] IS NOT NULL)