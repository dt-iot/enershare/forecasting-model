SELECT [DateTime], [Value], [TagName]
FROM [Runtime].[dbo].[History] 
Where (Tagname='Power_Q_3_7_0_W6.CV')
    and ([DateTime] >= '2023-12-01 00:00:00' and [DateTime] < '2023-12-15 00:00:00')
    and ([Value] IS NOT NULL)