SELECT [DateTime], [Value], [TagName]
FROM [Runtime].[dbo].[History]
Where (TagName = 'pmu')
    and ([DateTime] >= '2024-01-01 00:00:00' and [DateTime] < '2024-03-31 00:00:00')
    and ([Value] IS NOT NULL)