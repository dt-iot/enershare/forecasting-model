# ENERSHARE Use Case 5b - Demand Forecasting Model for Pilot 5: ASM Facilty

## Overview

This project implements a **Forecasting** designed to predict the demand of the ASM Facility based on historical data. The model leverages a **Multi-Layer Perceptron (MLP)** architecture built with TensorFlow/Keras to process input features and generate forecasts. The project includes scripts for training, evaluating, and making predictions with the model.

## Project Structure

- **forecasting-model**
    - *notebooks*: Contains the experimental notebooks used to evaluate model performance.
    - *Queries*: Contains the SQL queries used to download historical data.
    - *resources*: Contains the historical data.
    - **service**: Contains the forecasting service.
        - *data_acquisition*: Contains the service to automatically download previous day historical data.
        - *model*: Contains the MLP Model. It enables the re-training of the model.
        - *preprocessing*: Contains the module that enables the preprocessing of data
        - *app.py*: This file contains the API that enables the exposure of the service
        - *config.py*: Contains the global config variables
        - *utils.py*: Contains supporting functions
        - *Dockerfile*: To build the docker image 
    - *.gitignore*
    - *LICENCE.md*
    - *README.md*

## Setup and Installation

The service can be installed locally using Docker, building a docker container using the procedure desribed in the following sections

### Prerequisites

For a complete list of requirements, please refer to the requirements.txt file available in the forecasting-model/service folder.

Ensure you have the following installed:
- Python 3.x  (higher than 3.10 is preferred)
- TensorFlow/Keras
- Pandas
- FastAPI (for the API)
- Docker and docker-compose

#### Local installation

To install the service locally, without using Docker, open a terminal window navigate into the `forecasting-model/service folder`:

```
pip install -r requirements.txt
```

This command should install all the required libraries for the service to work.
After this to run the service locally, simply run the following command in the terminal:

```
python app.py
```

If everything is installed correctly, the service should be available on `http://localhost:8000`

#### Docker installation

To install the service using docker, be sure to have it installed in you local machine. You can check the functioning of docker by running in the terminal

```
docker --version
```

Navigate into the `forecasting-model/service folder` and write the following command in the terminal:

```
docker build -t forecasting-service .
```

This will build the image on your local docker instance calling it "forecasting-service". Then, to run the service as a docker container run:

```
docker run -p 8000_8000 -d --name forecasting-container forecasting-service
```

The command will map port 8000 inside the container to your local 8000 port, you can verify its functioning by connecting to `http://localhost:8000/docs`. 

## API Endpoints

Questa sezione descrive i metodi esposti dall'API e le relative funzionalità.

### 1. `POST /auth`
Authenticates a user on a connector and returns an access token.

- **Metodo**: `POST`
- **URL**: `/auth`
- **Descrizione**: Verifies the user’s credentials and returns a token to authenticate future requests.
- **Parametri**:
  - `username` (string): username.
  - `password` (string): password.
- **Risposta**:
  - `200 OK`: Auth Token.
  - `400 Bad Request`: Invalid credentials or user not found.

---

### 2. `POST /last_execution_results`
Sends the results of the last execution to the connector.

- **Metodo**: `POST`
- **URL**: `/last_execution_results`
- **Descrizione**: Sends the results of the model’s last execution, including files and metadata, to a remote endpoint.
- **Parametri**: None.
- **Risposta**:
  - `200 OK`: Request completed successfully.
  - `500 Internal Server Error`: Error sending the data.

---

### 3. `GET /forecast`
Returns the results of the last saved forecast.

- **Metodo**: `GET`
- **URL**: `/forecast`
- **Descrizione**: Retrieves forecast data from the model, including timestamps and forecast values.
- **Parametri**: None.
- **Risposta**:
  - `200 OK`: JSON with the forecast results.
  - `500 Internal Server Error`: Error retrieving the data.

---

### 4. `GET /get_metrics`
Returns the model's performance metrics.

- **Metodo**: `GET`
- **URL**: `/get_metrics`
- **Descrizione**: Provides the evaluation metrics of the trained model, specifically MSE and R-squared.
- **Parametri**: None.
- **Risposta**:
  - `200 OK`: JSON containing the model’s metrics.
  - `500 Internal Server Error`: Error calculating the metrics. Check the service's logs. 

---

### 5. `GET /train_model`
Trains the MLP model. A pretrined instance of the MLP is available, so use this method only if new data is available to improve the its quality.

- **Metodo**: `GET`
- **URL**: `/train_model`
- **Descrizione**: Trains the ML model using historical datasets retrieved from the database.
- **Parametri**: None (input data is hardcoded in the code).
- **Risposta**:
  - `200 OK`: {"Model": "Trained"}.
  - `500 Internal Server Error`:  Error during the model training. Check the logs for a detailed description. 

---

### 6. `GET /predictions`
Returns predictions made by a pre-trained model.

- **Metodo**: `GET`
- **URL**: `/predictions`
- **Descrizione**: Returns a JSON with demand predictions using a pre-trained MLP model.
- **Parametri**: None.
- **Risposta**:
  - `200 OK`: JSON with forecast data. The model automatically saves the data in a CSV file stored in the Docker container, making it easy to retrieve.
  - `500 Internal Server Error`: Error in the prediction process. Check the logs for further details. 


## Future Work

The model needs to be improved by adding the production forecastings, using the same infrastructure. 
Also, the model will be connected to the optimizer service that will be developed in the future, to calculate the energy surplus available to be shared. 

## Licence

The model is licenced under MIT Licence. See the LICENCE file for details.

## Acknowledgments

Thanks to ASM Terni for providing the historical data to train the forecasting model. 
For any further information on the service, please contact marzia.mammina@eng.it or lorenzo.cristofori@eng.it.







