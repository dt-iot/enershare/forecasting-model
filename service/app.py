import os
import time
import logging
import json
import threading
import requests

from pydantic import BaseModel
import pandas as pd

from fastapi import FastAPI, Request, Header
from concurrent.futures import ThreadPoolExecutor

from model.main import MLModel
from preprocessing.main import Dataset

from data_acquisition.main import SQLQuery

logging.basicConfig(
    level=logging.INFO,  # Imposta il livello di log a DEBUG
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',  # Formato del messaggio di log
    datefmt='%Y-%m-%d %H:%M:%S',  # Formato della data
    handlers=[
        logging.FileHandler('app.log'),  # Scrive i log in un file
        logging.StreamHandler()  # Mostra i log sulla console
    ]
)

app = FastAPI()

class Variables(BaseModel):
    
    username: str
    password: str

logging.basicConfig(
    level=logging.INFO,  # Livello minimo di registrazione
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',  # Formato del messaggio di log
    datefmt='%Y-%m-%d %H:%M:%S'  # Formato della data e dell'ora
)

# SQL Queries
# Temperature Query, see data_acquisition module
logging.info("Downloading Temperature Data")
temperature_df = SQLQuery().temperatureDataQuery()

# Humidity Query, see data_acquisition module
logging.info("Downloading Humidity Data")
humidity_df = SQLQuery().humidityDataQuery()

# ActivePowerW4 Query, see data_acquisition module
logging.info("Downloading ActivePowerW4 Data")
active_power_df_w4  = SQLQuery().powerDataQuery[0]

# ReactivePowerW4 Query, see data_acquisition module
logging.info("Downloading ReactivePowerW4 Data")
reactive_power_df_w4 = SQLQuery().powerDataQuery[1]

# ActivePowerW2 Query, see data_acquisition module
logging.info("Downloading ActivePowerW2 Data")
active_power_df_w2 = SQLQuery().powerDataQuery[2]

# ReactivePowerW2 Query, see data_acquisition module
logging.info("Downloading ReactivePowerW2 Data")
reactive_power_df_w2 = SQLQuery().powerDataQuery[3]

# Noise Query, see data_acquisition module
logging.info("Downloading Noise Data")
noise_df = SQLQuery().noiseDataQuery()

@app.get("/")
async def status():
    
    return {"API":"Connected"}

# Authenticate in the connector
@app.post("/auth")
async def status(user_data: Variables):
    
    """
    Authenticate the user by sending their credentials to the authentication endpoint.

    Args:
    - user_data (Variables): An object containing the user's `username` and `password`.

    Returns:
    - 200: If authentication is successful, returns a token.
    - 400: If authentication fails (e.g., incorrect credentials or user not found).
    
    Note:
    The actual response code for failures should be handled in the logic, depending on the 
    authentication service response (currently not handled here).
    """
    
    login_url = r"http://128.140.45.133:15502/api/user/auth/token"
    body = {
        "username": user_data.username,
        "password":user_data.password
    }
    
    res = requests.post(login_url, json=body)
    print(res.content)
    
    token = res.content[0]
    print(token)
    
    return 200

# Post Historical Forecasts on the Connector    
@app.post("/last_execution_results")
async def last_execution_results():
    
    """
    Sends the results of the last execution to a specified data endpoint.

    This method prepares a JSON payload containing details about the file with 
    the results of the last execution, such as file name, data catalog ID, description, 
    and other metadata. It then sends this information to a remote API endpoint using 
    a POST request. 

    The payload includes:
    - File path and name (`fileName`)
    - Data catalog ID (`data_catalog_data_offerings_id`)
    - Description of the data (`description`, `title`)
    - File size and content (`fileSize`, `message`)
    - Metadata about the provider, including provider ID, URLs for FIWARE, broker, ECC, and ED API.

    Args:
    - None

    Returns:
    - dict: A dictionary indicating the completion status of the request and the 
      response from the API.

    Note:
    - The `token` used for authentication is hardcoded. In a production scenario, it should be
      securely retrieved, possibly using a separate authentication service.
    """
    
    token = "token"  
    
    body = {
            "data_send": {
                "fileName": r"C:\Users\annatalini\OneDrive - Engineering Ingegneria Informatica S.p.A\ENERSHARE\forecasting-model\resources\results\eval_df.csv",
                "data_catalog_data_offerings_id": "bf134e24-85d8-4a7e-9eeb-ea1c4513f1ae",
                "description": "descr",
                "title": "Result Data",
                "fileSize": 4,
                "message": "data:text/plain;base64,dGVzdA==",
                "sub-entities": {
                "provider": {
                    "id": "603b87ac-6139-4a94-8742-8142e04f842c",
                    "broker_url": "http://128.140.45.133:1026",
                    "ecc_url": "https://128.140.45.133:8889/data",
                    "provider_fiware_url": "https://be-dataapp-provider:8083",
                    "ed_api_url": "http://128.140.45.133:30001/api"
                }
                },
                "created_by": "603b87ac-6139-4a94-8742-8142e04f842c"
            }
        }
    
    header = {"Content-Type": "application/json",
              "Authorization": f"Bearer {token}"}
    
    url = r"http://128.140.45.133:8080/api/entity?id=bf134e24-85d8-4a7e-9eeb-ea1c4513f1ae"
    
    r = requests.post(url, json=body, headers=header)
    status_code = r.status_code
    
    return ({
        "Completed" :f"{r}"
    })
    
# Return Historical Forecasts
@app.get("/forecast")
async def get_forecast():
    
    """
    Retrieves forecast data from a CSV file and returns it as a JSON response.

    This method reads forecast data from a local CSV file and constructs a JSON response
    containing the demand forecast. The data includes:
    - Timestamps (`ds`) for the forecasted periods.
    - Forecasted values (`y_pred`) for energy demand.

    The response is structured as:
    - `demand`: A list of dictionaries containing timestamps and forecasted values.
    - `production`: An empty list (presumably for future data related to energy production).

    Args:
    - None

    Returns:
    - dict: A JSON response with forecasted energy demand data and an empty production list.

    Note:
    - The CSV file is read from a local path, so this method assumes the file exists
      and is accessible in the specified location.
    """
    
    forecast = pd.read_csv(r"C:\Users\annatalini\OneDrive - Engineering Ingegneria Informatica S.p.A\ENERSHARE\forecasting-model\resources\results\eval_df.csv")
        
    solution = {
        "results": {
            "demand": [
                {"timestamps": forecast['ds']},
                {"forecasts": forecast['y_pred']}
            ],
            "production" :[]
        }
    }
    
    return solution

# Return Model Metrics
@app.get("/get_metrics")
def ml_model():
    
    """
    Retrieves performance metrics of the forecasting model after training.

    This method performs the following steps:
    1. Creates a training set using the `Dataset.create_training_set()` method.
    2. Trains a machine learning model (a multi-layer perceptron) using data from a historical CSV file.
    3. Extracts the model's performance metrics and predictions.

    The model is trained on a dataset located at the specified path, and the method 
    returns the evaluation metrics of the trained model.

    Args:
    - None

    Returns:
    - dict: A dictionary or object containing the model's performance metrics.
    
    Note:
    - The path to the training set is hardcoded, so it assumes that the CSV file exists 
      and is accessible at the specified location.
    """
    
    training_set = Dataset.create_training_set()
    
    instance = MLModel.multi_layer_perceptron(r"C:\Users\annatalini\OneDrive - Engineering Ingegneria Informatica S.p.A\ENERSHARE\forecasting-model\service\resources\training_set.csv")
    metrics = instance[0]
    predicted = instance[1]
    
    return metrics

# Re-Train Model
@app.get("/train_model")
async def train_model() -> object:
    
    
    logging.info("Creating Training Set")
    training_set = Dataset.create_training_set()
    
    logging.info("Training ML Model")
    instance = MLModel.multi_layer_perceptron(r"C:\Users\annatalini\OneDrive - Engineering Ingegneria Informatica S.p.A\ENERSHARE\forecasting-model\service\resources\training_set.csv")
    metrics = instance[0]
    predicted = instance[1]
    
    return {"Model": "Trained"}

# Get Prediction
@app.get("/predictions")
def get_predictions():

    """
    Generates a prediction using a pre-trained machine learning model.

    This method performs the following steps:
    1. Loads a pre-trained neural network model from a `.h5` file using TensorFlow/Keras.
    2. Prepares a set of test input data representing features from the previous day.
    3. Uses the loaded model to generate predictions based on the input data.

    Args:
    - None

    Returns:
    - numpy.ndarray: The prediction generated by the machine learning model for the given input data.
    
    Note:
    - The test data is hardcoded and should be replaced with dynamic data in a production scenario.
    - The model file path is also hardcoded and assumes the model file exists and is accessible at the specified location.
    """
    
    from tensorflow.keras.models import load_model
    
    # inserire qui i dati del giorno precedente
    test = [52.715127, -11.119251, 0.827931, -1.074355, 53.875063, 1.356358, 53.892134, 50.194087, 57.750000]
    
    ml_model = load_model(r'C:\Users\annatalini\OneDrive - Engineering Ingegneria Informatica S.p.A\ENERSHARE\forecasting-model\service\resources\models\TrainedModel.h5')
    prediction = ml_model.predict(test)
    
    return prediction
    
    
if __name__ == "__main__":
    
    import uvicorn
    uvicorn.run("app:app", host="0.0.0.0", port=8000, reload=False)