import logging
import os
import mlflow
import pickle

import mlflow.keras

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, mean_absolute_error
import numpy as np
import pandas as pd

logging.basicConfig(
    level=logging.INFO,  # Imposta il livello di log a DEBUG
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',  # Formato del messaggio di log
    datefmt='%Y-%m-%d %H:%M:%S',  # Formato della data
    handlers=[
        logging.FileHandler('app.log'),  # Scrive i log in un file
        logging.StreamHandler()  # Mostra i log sulla console
    ]
)

class MLModel:
    
    def __init__(self) -> None:
        pass
    
    def multi_layer_perceptron(dataset: str):
        
        training_set = pd.read_csv(dataset)
        logging.info(training_set.head(5))
        
        def create_sequences(data, time_steps=10):
            X, y = [], []
            for i in range(time_steps, len(data)):
                X.append(data[i-time_steps:i, :-1])  # features
                y.append(data[i, -1])  # target (DEMAND)
            return np.array(X), np.array(y)
        
        logging.info("Starting Processing")
        
        features = ['Power_P_1_7_0_W4.CV', 'Power_P_1_7_0_W2.CV', 'Power_Q_3_7_0_W4.CV', 'Power_Q_3_7_0_W2.CV', 'temperature', 'noise', 'APP_W4', 'APP_W2', 'humidity']
        target = 'DEMAND'
        
        logging.info("Scaling Data")
        mlp_scaler = MinMaxScaler()
        scaled_data_mlp = mlp_scaler.fit_transform(training_set[features + [target]])
        
        X_mlp = scaled_data_mlp[:, :-1]  # Le features
        y_mlp = scaled_data_mlp[:, -1]   # Il target
        
        X_train_mlp, X_test_mlp, y_train_mlp, y_test_mlp = train_test_split(X_mlp, y_mlp, test_size=0.2, shuffle=False)
        
        # Creazione del modello MLP
        mlp = Sequential()
        mlp.add(Dense(64, input_dim=X_train_mlp.shape[1], activation='relu'))
        mlp.add(Dropout(0.2))
        mlp.add(Dense(64, activation='relu'))
        mlp.add(Dense(1))

        # Compilazione del modello
        mlp.compile(optimizer='adam', loss='mse')

        #Summary
        logging.info(f"MLP summary {mlp.summary()}")
        
        logging.info("Starting Training")
        # with mlflow.start_run():
    
            # Logga i parametri del modello
            # mlflow.log_param("optimizer", "adam")
            # mlflow.log_param("loss", "mse")
            # mlflow.log_param("batch_size", 32)
            # mlflow.log_param("epochs", 100)
            
        mlp_history = mlp.fit(X_train_mlp, y_train_mlp, epochs=100, batch_size=32, validation_data=(X_test_mlp, y_test_mlp))
            
            # Logga le metriche
            # for epoch, metrics in enumerate(mlp_history.history.items()):
            #     for metric_name, metric_values in metrics:
            #         mlflow.log_metric(metric_name, metric_values[-1], step=epoch)
            
            # Logga il modello
            #   mlflow.keras.log_model(mlp, "MLP")
            
            # with open(r"service/resources/models/TrainedModel.pkl", "wb") as file:
            #     pickle.dump(mlp, file)
            
        logging.info("Predictions")
        y_pred_mlp = mlp.predict(X_test_mlp)
        
        mlp_scaler_target = MinMaxScaler()
        mlp_scaler_target.min_, mlp_scaler_target.scale_ = mlp_scaler.min_[-1], mlp_scaler.scale_[-1]
        y_test_mlp_rescaled = mlp_scaler_target.inverse_transform(y_test_mlp.reshape(-1, 1)).reshape(-1)
        y_pred_mlp_rescaled = mlp_scaler_target.inverse_transform(y_pred_mlp).reshape(-1)
        
        mlp_mse = mean_squared_error(y_test_mlp_rescaled, y_pred_mlp_rescaled)
        mlp_mae = mean_absolute_error(y_test_mlp_rescaled, y_pred_mlp_rescaled)
        print(f"Mean Squared Error: {mlp_mse}")
        print(f"Mean Absolute Error: {mlp_mae}")

        # mlflow.log_metric("Mean Squared Error", mlp_mse)
        # mlflow.log_metric("Mean Absolute Error", mlp_mae)
        
        
        metrics = {
            "Mean Squared Error:": mlp_mse,
            "Mean Absolute Error": mlp_mae
        }
        
        # mlflow.end_run()
        
        return metrics, y_pred_mlp
        
        
if __name__ == "__main__":
    
    from preprocessing.main import Dataset
    
    training_set = Dataset.create_training_set()
    
    instance = MLModel.multi_layer_perceptron(training_set)
    metrics = instance[0]
    predicted = instance[1]
    
    print(predicted.head())
            
            
            

            
            
            
            
            
        
        
        