import os

import pyodbc
import pandas as pd

from dotenv import load_dotenv
from datetime import datetime, timedelta


class SQLQuery:
    
    def __init__(self):
        
        self.today = datetime.now()
        self.yesterday = self.today - timedelta(days=1)
        self.today = self.today.strftime('%Y-%m-%d %H:%M:%S')
        self.yesterday = self.yesterday.strftime('%Y-%m-%d %H:%M:%S')
        print(self.today)
        print(self.yesterday)
        
        # Load Server Variables
        dotenv_path = r"C:\Users\annatalini\OneDrive - Engineering Ingegneria Informatica S.p.A\ENERSHARE\forecasting-model\service\.env"
        load_dotenv(dotenv_path)

        self.username = os.getenv("SQL_USERNAME")
        self.password = os.getenv("SQL_PASSWORD")
        self.server = os.getenv('SQL_SERVER')
        self.database = os.getenv("DATABASE")
        
        print(self.username)
        print(self.password)
        print(self.server)
        print(self.database)

        self.driver = '{ODBC Driver 18 for SQL Server}'
    
    def powerDataQuery(self):
        
        """Query to obtain power data"""

        conn = pyodbc.connect(
            f'DRIVER={self.driver};SERVER={self.server};DATABASE={self.database};UID={self.username};PWD={self.password};TrustServerCertificate=yes;'
        )

        W4_active_query = f"""
        SELECT [DateTime], [Value], [TagName]
        FROM [Runtime].[dbo].[History] 
        Where (Tagname='Power_P_1_7_0_W4.CV')
            and ([DateTime] >= '{self.yesterday}' and [DateTime] < '{self.today}')
            and ([Value] IS NOT NULL)
        """
        
        W4_reactive_query = f"""
        SELECT [DateTime], [Value], [TagName]
        FROM [Runtime].[dbo].[History] 
        Where (Tagname='Power_Q_3_7_0_W4.CV')
            and ([DateTime] >= '{self.yesterday}' and [DateTime] < '{self.today}')
            and ([Value] IS NOT NULL)
        """
        
        W2_active_query = f"""
        SELECT [DateTime], [Value], [TagName]
        FROM [Runtime].[dbo].[History] 
        Where (Tagname='Power_P_1_7_0_W2.CV')
            and ([DateTime] >= '{self.yesterday}' and [DateTime] < '{self.today}')
            and ([Value] IS NOT NULL)
        """
        
        W2_reactive_query = f"""
        SELECT [DateTime], [Value], [TagName]
        FROM [Runtime].[dbo].[History] 
        Where (Tagname='Power_Q_3_7_0_W2.CV')
            and ([DateTime] >= '{self.yesterday}' and [DateTime] < '{self.today}')
            and ([Value] IS NOT NULL)
        """

        active_power_w4 = pd.read_sql(W4_active_query, conn)
        reactive_power_w4 = pd.read_sql(W4_reactive_query, conn)
        active_power_w2 = pd.read_sql(W2_active_query, conn)
        reactive_power_w2 = pd.read_sql(W2_reactive_query, conn)
        
        conn.close()

        # print(active_power_w4)
        
        return active_power_w4, reactive_power_w4, active_power_w2, reactive_power_w2
        
    def temperatureDataQuery(self):
        
        """Query to obtain temperature data"""

        conn = pyodbc.connect(
            f'DRIVER={self.driver};SERVER={self.server};DATABASE={self.database};UID={self.username};PWD={self.password};TrustServerCertificate=yes;'
        )

        query = f"""
        SELECT [DateTime], [Value], [TagName]
        FROM [Runtime].[dbo].[History]
        WHERE TagName IN (
            'Temperature_NCD1.Cv',
            'Temperature_NCD2.Cv',
            'Temperature_NCD3.Cv',
            'Temperature_NCD4.Cv',
            'Temperature_NCD5.Cv',
            'Temperature_NCD6.Cv',
            'Temperature_NCD7.Cv',
            'Temperature_NCD8.Cv',
            'Temperature_NCD9.Cv',
            'Temperature_NCD10.Cv'
        )
        AND ([DateTime] >= '{self.yesterday}' AND [DateTime] < '{self.today}')
        AND ([Value] IS NOT NULL)
        """

        temperature_df = pd.read_sql(query, conn)
        conn.close()

        # print(temperature_df)
        return temperature_df
        
    def humidityDataQuery(self):

        """Query to obtain humidity data"""

        conn = pyodbc.connect(
            f'DRIVER={self.driver};SERVER={self.server};DATABASE={self.database};UID={self.username};PWD={self.password};TrustServerCertificate=yes;'
        )

        query = f"""
        SELECT [DateTime], [Value], [TagName]
        FROM [Runtime].[dbo].[History] 
        WHERE (Tagname='Humidity_NCD1.Cv') 
            and (Tagname='Humidity_NCD10.Cv')
            and (Tagname='Humidity_NCD2.Cv')
            and (Tagname='Humidity_NCD3.Cv')
            and (Tagname='Humidity_NCD4.Cv')
            and (Tagname='Humidity_NCD5.Cv')
            and (Tagname='Humidity_NCD6.Cv')
            and (Tagname='Humidity_NCD7.Cv')
            and (Tagname='Humidity_NCD8.Cv')
            and (Tagname='Humidity_NCD9.Cv')
        and ([DateTime] >= '{self.yesterday}' and [DateTime] < '{self.today}')
        and ([Value] IS NOT NULL)
        """

        humidity_df = pd.read_sql(query, conn)
        conn.close()

        # print(humidity_df)
        return humidity_df
        
    
    def noiseDataQuery(self):
                
        """Query to obtain noise data"""

        conn = pyodbc.connect(
            f'DRIVER={self.driver};SERVER={self.server};DATABASE={self.database};UID={self.username};PWD={self.password};TrustServerCertificate=yes;'
        )

        query = f"""
        SELECT [DateTime], [Value], [TagName]
        FROM [Runtime].[dbo].[History] 
        Where (Tagname='med_dB_ENM_001635.Cv') 
            and (Tagname='med_dB_ENM_001635.Cv')
            and (Tagname='med_dB_ENM_001636.Cv')
            and (Tagname='med_dB_ENM_001637.Cv')
            and (Tagname='med_dB_ENM_001638.Cv')
            and (Tagname='med_dB_ENM_001639.Cv')
            and (Tagname='med_dB_ENM_001640.Cv')
            and (Tagname='med_dB_ENM_001641.Cv')
            and (Tagname='med_dB_ENM_001642.Cv')
            and (Tagname='med_dB_ENM_001643.Cv')
            and (Tagname='med_dB_ENM_001644.Cv')
            and ([DateTime] >= '{self.yesterday}' and [DateTime] < '{self.today}')
            and ([Value] IS NOT NULL)
        """

        noise_df = pd.read_sql(query, conn)
        conn.close()

        # print(noise_df)
        return noise_df
        
        
        
        
if __name__ == "__main__":
    
    power_df = SQLQuery().powerDataQuery()[0]
    print(power_df)
    print("---------------------")
    temp_df = SQLQuery().temperatureDataQuery()
    print(temp_df)
    print("---------------------")





