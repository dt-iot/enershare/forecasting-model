import math
import numpy as np

def create_sequences(data, time_steps=10):
    X, y = [], []
    for i in range(time_steps, len(data)):
        X.append(data[i-time_steps:i, :-1])  # features
        y.append(data[i, -1])  # target (DEMAND)
    return np.array(X), np.array(y)

def calc_apparent_power(active_power, reactive_power):
    
    active = active_power**2
    reactive = reactive_power**2
    
    apparent_power = math.sqrt(reactive + active)
    
    return apparent_power