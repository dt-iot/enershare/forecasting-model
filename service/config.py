# Datasets Paths
HISTORICAL_TEMPERATURE_DF = r"service/resources/Temperature_DEC23_1-15.csv"
HISTORICAL_HUMIDITY_DF = r"service/resources/Humidity_DEC23_1-15.csv"

ACTIVE_POWER_W4 = r"service/resources/ACT_Power_W4_DEC_2023.csv"
REACTIVE_POWER_W4 = r"service/resources/REC_Power_W4_DEC_2023.csv"
ACTIVE_POWER_W2 = r"service/resources/ACT_Power_W2_DEC_2023.csv"
REACTIVE_POWER_W2 = r"service/resources/REC_Power_W2_DEC_2023.csv"

HISTORICAL_NOISE_DF = r"service/resources/Noise_DEC23_1-15.csv"