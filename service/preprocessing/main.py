import os
import math
import logging

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

# from utils import calc_apparent_power

logging.basicConfig(
    level=logging.INFO,  # Imposta il livello di log a DEBUG
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',  # Formato del messaggio di log
    datefmt='%Y-%m-%d %H:%M:%S',  # Formato della data
    handlers=[
        logging.FileHandler('app.log'),  # Scrive i log in un file
        logging.StreamHandler()  # Mostra i log sulla console
    ]
)

class Dataset:
    
    def __init__():
        pass
    
    def create_training_set():
        
        def calc_apparent_power(active_power, reactive_power):
            
            logging.info("Calculating Apparent Power")
    
            active = active_power**2
            reactive = reactive_power**2
            
            apparent_power = math.sqrt(reactive + active)
            
            return apparent_power
                
        # Paths
        
        logging.info("Reading Datasets")
        temp_path = r"C:\Users\annatalini\OneDrive - Engineering Ingegneria Informatica S.p.A\ENERSHARE\forecasting-model\service\resources\Temperature_DEC23_1-15.csv"
        humidity_path = r"C:\Users\annatalini\OneDrive - Engineering Ingegneria Informatica S.p.A\ENERSHARE\forecasting-model\service\resources\Humidity_DEC23_1-15.csv"
        active_power_w4 = r"C:\Users\annatalini\OneDrive - Engineering Ingegneria Informatica S.p.A\ENERSHARE\forecasting-model\service\resources\ACT_Power_W4_DEC_2023.csv"
        reactive_power_w4 = r"C:\Users\annatalini\OneDrive - Engineering Ingegneria Informatica S.p.A\ENERSHARE\forecasting-model\service\resources\REC_Power_W4_DEC_2023.csv"
        active_power_w2 = r"C:\Users\annatalini\OneDrive - Engineering Ingegneria Informatica S.p.A\ENERSHARE\forecasting-model\service\resources\ACT_Power_W2_DEC_2023.csv"
        reactive_power_w2 = r"C:\Users\annatalini\OneDrive - Engineering Ingegneria Informatica S.p.A\ENERSHARE\forecasting-model\service\resources\REC_Power_W2_DEC_2023.csv"
        noise_path = r"C:\Users\annatalini\OneDrive - Engineering Ingegneria Informatica S.p.A\ENERSHARE\forecasting-model\service\resources\Noise_DEC23_1-15.csv"
        
        temperature_df = pd.read_csv(temp_path)
        humidity_df = pd.read_csv(humidity_path)
        active_power_df_w4 = pd.read_csv(active_power_w4)
        reactive_power_df_w4 = pd.read_csv(reactive_power_w4)
        active_power_df_w2 = pd.read_csv(active_power_w2)
        reactive_power_df_w2 = pd.read_csv(reactive_power_w2)
        df_noise = pd.read_csv(noise_path)
        
        # Temperature Calculations
        logging.info("Creating Temperature DF")
        temperature_df['Value'] = temperature_df['Value'].str.replace(",", ".")
        temperature_df['Value'] = temperature_df['Value'].astype(float)
        temperature_df['DateTime'] = pd.to_datetime(temperature_df['DateTime'])
        temperature_df = temperature_df.reindex(columns=temperature_df.columns[::-1])
        temperature_df.set_index(temperature_df['DateTime'], inplace=True)
        
        temperature_df = temperature_df.resample("30T").mean(numeric_only=True)
        
        # Humidity Calculations
        logging.info("Creating Humidity DF")
        humidity_df['Value'] = humidity_df['Value'].str.replace(",", ".")
        humidity_df['Value'] = humidity_df['Value'].astype(float)
        humidity_df['DateTime'] = pd.to_datetime(humidity_df['DateTime'])
        humidity_df = humidity_df.reindex(columns=humidity_df.columns[::-1])
        humidity_df.set_index(humidity_df['DateTime'], inplace=True)
        humidity_df = humidity_df.resample("30T").mean(numeric_only=True)
        
        #Power Calculations
        logging.info("Creating Power DF")
        active_power_df_w4['Value'] = active_power_df_w4['Value'].str.replace(",", ".").astype(float)
        reactive_power_df_w4['Value'] = reactive_power_df_w4['Value'].str.replace(",", ".").astype(float)
        active_power_df_w2['Value'] = active_power_df_w2['Value'].str.replace(",", ".").astype(float)
        reactive_power_df_w2['Value'] = reactive_power_df_w2['Value'].str.replace(",", ".").astype(float)
        # print("Done")
        logging.info("Mapping Power DF")
        active_power_df_w4['DateTime'] = pd.to_datetime(active_power_df_w4['DateTime'], dayfirst=False, format="mixed")
        reactive_power_df_w4['DateTime'] = pd.to_datetime(reactive_power_df_w4['DateTime'], dayfirst=False, format="mixed")
        active_power_df_w2['DateTime'] = pd.to_datetime(active_power_df_w2['DateTime'], dayfirst=False, format="mixed")
        reactive_power_df_w2['DateTime'] = pd.to_datetime(reactive_power_df_w2['DateTime'], dayfirst=False, format="mixed")
        
        new_names_ac_w4 = {"Value": "Power_P_1_7_0_W4.CV"}
        new_names_rc_w4 = {"Value": "Power_Q_3_7_0_W4.CV"}
        new_names_ac_w6 = {"Value": "Power_P_1_7_0_W2.CV"}
        new_names_rc_w6 = {"Value": "Power_Q_3_7_0_W2.CV"}

        ac_w4_dataset = active_power_df_w4.rename(columns=new_names_ac_w4)
        rc_w4_dataset = reactive_power_df_w4.rename(columns=new_names_rc_w4)
        ac_w2_dataset = active_power_df_w2.rename(columns=new_names_ac_w6)
        rc_w2_dataset = reactive_power_df_w2.rename(columns=new_names_rc_w6)
        
        logging.info("Indexing Power DF")
        ac_w4_dataset.set_index(ac_w4_dataset['DateTime'], inplace=True, drop=True)
        rc_w4_dataset.set_index(rc_w4_dataset['DateTime'], inplace=True, drop=True)
        ac_w2_dataset.set_index(ac_w2_dataset['DateTime'], inplace=True, drop=True)
        rc_w2_dataset.set_index(rc_w2_dataset['DateTime'], inplace=True, drop=True)
        
        ac_w4_dataset = ac_w4_dataset.drop(["DateTime", "TagName"], axis=1)
        rc_w4_dataset = rc_w4_dataset.drop(["DateTime", "TagName"], axis=1)
        ac_w2_dataset = ac_w2_dataset.drop(["DateTime", "TagName"], axis=1)
        rc_w2_dataset = rc_w2_dataset.drop(["DateTime", "TagName"], axis=1)
        
        dataset = pd.concat([ac_w4_dataset, rc_w4_dataset, ac_w2_dataset, rc_w2_dataset], ignore_index=False)
        dataset_resampled = dataset.resample("30T").mean()
        
        dataset_resampled['APP_W4'] = dataset_resampled.apply(lambda row: calc_apparent_power(row["Power_P_1_7_0_W4.CV"], row['Power_Q_3_7_0_W4.CV']), axis=1)
        dataset_resampled['APP_W2'] = dataset_resampled.apply(lambda row: calc_apparent_power(row["Power_P_1_7_0_W2.CV"], row['Power_Q_3_7_0_W2.CV']), axis=1)
        
        dataset_resampled['DEMAND'] = dataset_resampled.apply(lambda row: calc_apparent_power(row["APP_W2"], row['APP_W4']), axis=1)
        
        # Noise Calculations
        logging.info("Creating Noise DF")
        df_noise = df_noise.rename(columns= {"Value": "noise"})
        df_noise['DateTime'] = pd.to_datetime(df_noise['DateTime'])
        df_noise.set_index(df_noise['DateTime'], inplace = True)
        df_noise = df_noise[["noise", "TagName", "DateTime"]]
        df_noise = df_noise.resample("30T").mean(numeric_only=True) 
        
        
        # Final DF
        logging.info("Creating Input DF")
        df_merged = pd.merge(dataset_resampled, humidity_df, on="DateTime")
        df_merged = pd.merge(df_merged, temperature_df, on="DateTime")
        df_merged = pd.merge(df_merged, df_noise, on="DateTime")


        df_merged = df_merged.rename(columns={"Value_x": "humidity",
                                            "Value_y": "temperature", 
                                            "Value": "noise"})
        
        # Variables
        df_merged['APP_W4_Rolling'] = df_merged['APP_W4'].rolling(window=15).mean()
        df_merged['APP_W2_Rolling'] = df_merged['APP_W2'].rolling(window=15).mean()
        df_merged['TEMPERATURE_Rolling'] = df_merged['temperature'].rolling(window=15).mean()
        df_merged['DEMAND_Rolling'] = df_merged['DEMAND'].rolling(window=15).mean()
        df_merged['DEMAND_Diff'] = df_merged['DEMAND'].diff()
        
        plt.figure(figsize=(20, 7.5))
        plt.figure()
        df_merged.plot(figsize=(20, 7.5)) 
        plt.savefig(r"C:\Users\annatalini\OneDrive - Engineering Ingegneria Informatica S.p.A\ENERSHARE\forecasting-model\service\resources\plots\plot_analysis.png")
        
        
        df_merged.to_csv(r"C:/Users/annatalini/OneDrive - Engineering Ingegneria Informatica S.p.A/ENERSHARE/forecasting-model/service/resources/training_set.csv", index=True)
        logging.info("Returning DF")
        
        
        return df_merged
           
# if __name__ == "__main__":
    
#     print(Dataset.create_training_set())
        
        
        
        
        
        
        